package com.huiapp.employeeManager.repo;

import com.huiapp.employeeManager.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployeeRepo extends JpaRepository<Employee,Long> {
    //Define Query Methods
    void deleteEmployeeById(Long id);
    Optional<Employee> findEmployeeById(Long id);
}
