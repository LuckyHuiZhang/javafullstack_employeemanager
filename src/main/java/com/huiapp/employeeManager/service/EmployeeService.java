package com.huiapp.employeeManager.service;

import com.huiapp.employeeManager.exception.UserNotFoundException;
import com.huiapp.employeeManager.model.Employee;
import com.huiapp.employeeManager.repo.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class EmployeeService {
    private final EmployeeRepo employeeRepo;

    @Autowired
    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    //Add Employee
    public Employee addEmployee(Employee employee) {
        employee.setEmployeeCode(UUID.randomUUID().toString());
        return employeeRepo.save(employee);
    }

    //Find All Employees
    public List<Employee> findAllEmployees() {
        return employeeRepo.findAll();
    }

    //Update Employee
    public Employee updateEmployee(Employee employee) {
        return employeeRepo.save(employee);
    }

    //Find Employee By Id
    public Employee findEmployeeById(Long id) {
        return employeeRepo.findEmployeeById(id)
                .orElseThrow(() -> new UserNotFoundException("User by id " + id + " was not found"));
    }

    //Delete Employee By Id
    public void deleteEmployee(Long id){
        employeeRepo.deleteEmployeeById(id);
    }
}
